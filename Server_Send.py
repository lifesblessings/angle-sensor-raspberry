import redis
import requests
import json
import time
from JsonManager import *
from serials import serial_send

def post_server():
    global red
    # api-endpoint 
    try:
        cm = JsonManager(os.path.join(os.path.dirname(__file__), "config_params.json"))
        URL = cm.data["URL"] 
        values = red.lpop('tosend')
        values = values.decode('utf8').replace("'", '"')
        # sending get request and saving the response as response object
        serial_send(json.dumps(values).encode()) 
        r = requests.post(URL, values) 
        # extracting data in json format 
        if r:
            print("response received")
            if r.status_code == "OK":
                print("success")
        else:
            print("null data")
            red.lpush(json.dumps(values))
            time.sleep(1)
            
    except Exception as e:
        print("error sending",e)

def main():
    global red
    red = redis.StrictRedis('localhost')
    while True:
        if red.llen('tosend')>0:
                post_server()

if __name__ == "__main__":
    main()