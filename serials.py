from serial import *


def serial_send(data):
    ser = Serial()
    ser.port = "/dev/ttyUSB0"
    # ser.port = "/dev/ttyS2"
    ser.baudrate = 9600
    ser.bytesize = EIGHTBITS  # number of bits per bytes
    ser.parity = PARITY_NONE  # set parity check: no parity
    ser.stopbits = STOPBITS_ONE  # number of stop bits
    # ser.timeout = None          #block read
    ser.timeout = 1  # non-block read
    # ser.timeout = 2              #timeout block read
    ser.xonxoff = False  # disable software flow control
    ser.rtscts = False  # disable hardware (RTS/CTS) flow control
    ser.dsrdtr = False  # disable hardware (DSR/DTR) flow control
    ser.writeTimeout = 2  # timeout for write

    try:
        ser.open()
    except Exception as e:
        print("error open serial port: " + str(e))
        exit()

    if ser.isOpen():

        try:
            ser.flushInput()  # flush input buffer, discarding all its contents
            ser.flushOutput()  # flush output buffer, aborting current output
                 # and discard all that is in buffer

            # write data
            ser.write(data)
            print("write data:", data)

            time.sleep(1)  #give the serial port sometime to receive the data

            numOfLines = 0

            while True:
                response = ser.readline()
                print("read data: " + response)
                numOfLines = numOfLines + 1
                if (numOfLines >= 1):
                    break

            ser.close()

        except Exception as e1:
            print("error communicating...: " + str(e1))

    else:
        print("cannot open serial port ")
