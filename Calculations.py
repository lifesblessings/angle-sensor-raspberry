import json
import math

def HextoDecimal(fragment):
    integer = int(fragment[:2], 16)
    decimal = int(fragment[-2:], 16)/256
    if(integer > 127):
        return integer -256 + decimal
    else:
        return integer + decimal

def get_angle2(ux, uy, vx, vy):
    costheta = (ux * vx + uy * vy) / \
               (math.sqrt(ux ** 2 + uy ** 2) * math.sqrt(vx ** 2 + vy ** 2))
    if costheta > 1.0:
        costheta = 1.0
    if costheta < -1.0:
        costheta = -1.0
    theta = math.acos(costheta)
    if ux * vy - uy * vx < 0.0:
        theta = -theta
    return theta

def calculate(sens_dict,ref_dict):
    sens_dict = sens_dict.decode('utf8').replace("'", '"')
    ref_dict = ref_dict.decode('utf8').replace("'", '"')
    sens_dict = json.loads(sens_dict)
    ref_dict = json.loads(ref_dict)
    count = 0
    xval = 0
    yval = 0
    sens_rssi = 0
    ref_rssi = 0
    xval_ref = 0
    yval_ref = 0
    for x,y,z,xr,yr,zr,sref,rref in zip(sens_dict["xdata"],sens_dict["ydata"],sens_dict["zdata"],ref_dict["xdata"],ref_dict["ydata"],ref_dict["zdata"],sens_dict["rssi"],ref_dict["rssi"]):
        xval += x
        yval += y
        xval_ref += xr
        yval_ref += yr
        sens_rssi += sref
        ref_rssi += rref
        count += 1
    
    return (sens_rssi/count),(ref_rssi/count),(xval/count),(yval/count),(xval_ref/count),(yval_ref/count)