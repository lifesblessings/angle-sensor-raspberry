import json
import os

class JsonManager:
    def __init__(self, path):
        with open(path)  as json_data:
            self.data = json.load(json_data)
            self.path = path
            json_data.close();

    def getConfig(self, path):
        return self.data;

    def updateJsonFile(self):
        #print '\n updating configuration ...:',self.config,' to ',self.path, '\n'
        with open(self.path, "w")  as file:
            json.dump(self.data, file, indent=2)
            file.close();
