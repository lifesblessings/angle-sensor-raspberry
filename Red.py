import redis
from datetime import datetime as dt
from JsonManager import *
import json
from Calculations import *

def redis_save(my_json):
    r = redis.StrictRedis('localhost')
    cm = JsonManager(os.path.join(os.path.dirname(__file__), "config_params.json"))
    beamer = cm.data["beamer_MAC"]
    for js in my_json:
        if js["type"] == 'Gateway':
            if js["mac"]!=beamer:
                break
            else:
                beamId=js["mac"]
        elif beamId and len(js["rawData"]) == 52 and js["mac"] == "".join(reversed([js["rawData"][40:][i:i+2] for i in range(0, len(js["rawData"][40:]), 2)])):
            js["timestamp"] = dt.strptime(js["timestamp"], '%Y-%m-%dT%H:%M:%SZ')
            sensId = js["mac"]
            #battery = int(js["rawData"][26:28], 16)
            xaccel = HextoDecimal(js["rawData"][28:32])
            yaccel = HextoDecimal(js["rawData"][32:36])
            zaccel = HextoDecimal(js["rawData"][36:40])
            print("x,y,z",xaccel,yaccel,zaccel)
            try:
                value_dict = r.get(sensId)
                value_dict = value_dict.decode('utf8').replace("'", '"')
                #acquire lock for dictionary
                have_lock = False
                my_lock = r.lock(sensId + "lock")
                try:
                    have_lock = my_lock.acquire(blocking=False)
                    while not have_lock:
                        print("Waiting lock...")
                        sleep(0.1)                                
                    value_dict = json.loads(value_dict)
                    value_dict["xdata"].append(xaccel)
                    value_dict["ydata"].append(yaccel)
                    value_dict["zdata"].append(zaccel)
                    value_dict["rssi"].append(js["rssi"])
                    value_dict = json.dumps(value_dict)
                    r.set(sensId,value_dict)
                finally:
                    if have_lock:
                        my_lock.release()
            
            except Exception as e:
                print("error",e)
                pass

            finally:
                count = r.get("count")
                count = int(count)+1
                if count>10:
                    send_data()
                    count = 0
                r.set("count",count)
                    

            
def send_data():
    print("sending data to redis")
    r = redis.StrictRedis('localhost')
    cm = JsonManager(os.path.join(os.path.dirname(__file__), "config_params.json"))
    beamer = cm.data["beamer_MAC"]
    for mac, ref in zip(cm.data["lever_MACs"],cm.data["reference_MACs"]):
        have_lock = False
        have_lock2 = False
        my_lock = r.lock(mac + "lock")
        my_lock2 = r.lock(ref + "lock")
        try:
            have_lock = my_lock.acquire(blocking=False)
            have_lock2 = my_lock2.acquire(blocking=False)
            while not have_lock and not have_lock2:
                print("Waiting lock...")
                sleep(0.1) 
            print("calculating...")                
            sens_rssi,ref_rssi,sens_x,sens_y,ref_x,ref_y = calculate(r.get(mac),r.get(ref))
            print("sens",sens_rssi)
            print("ref",ref_rssi)
            new_dict = {'xdata':[],'ydata':[],'zdata':[],'rssi':[]}
            r.set(mac,json.dumps(new_dict))
            r.set(ref,json.dumps(new_dict))
        finally:
            if have_lock:
                my_lock.release()
                my_lock2.release()
        print("sending...")
        redict = {'beamerId':beamer,'mac':mac,'ref_mac':ref,'sens_x':sens_x,'sens_y':sens_y,'ref_x':ref_x,'ref_y':ref_y,'sens_rssi':sens_rssi,'ref_rssi':ref_rssi,'time':dt.now().strftime(r"%Y-%m-%d %H:%M:%S")}
        r.rpush('tosend',json.dumps(redict))