import redis
from time import sleep
import redis

r = redis.StrictRedis()

have_lock = False
my_lock = r.lock("asd")

try:

    have_lock = my_lock.acquire(blocking=False)
    while not have_lock:
        print("Waiting lock. Doing some stuff...")
        sleep(1)    
    print("Acquired lock.")
    
finally:
    if have_lock:
        my_lock.release()