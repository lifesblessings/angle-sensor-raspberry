import json
import tornado.ioloop
import tornado.web
from os import *
from datetime import datetime as dt
from Red import redis_save
import redis
from JsonManager import *

class Post(tornado.web.RequestHandler):
    def get(self):
        form = """<form method="post">
        <input type="text" name="username"/>
        <input type="text" name="designation"/>
        <input type="submit"/>
        </form>"""
        self.write(form)

    def post(self):
        global beamer
        try:
            my_json = self.request.body.decode('utf8').replace("'", '"')
            my_json = json.loads(my_json)
            redis_save(my_json)
            
        except Exception as e:
            print("exception:",e)

application = tornado.web.Application([
    (r"/post", Post),
])

if __name__ == "__main__":
    r = redis.StrictRedis('localhost')
    cm = JsonManager(os.path.join(os.path.dirname(__file__), "config_params.json"))
    r.set("count",0)
    for mac, ref in zip(cm.data["lever_MACs"],cm.data["reference_MACs"]):
        new_dict = {'xdata':[],'ydata':[],'zdata':[],'rssi':[]}
        r.set(mac, json.dumps(new_dict))
        r.set(ref, json.dumps(new_dict))
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()