import json
import os
import requests
from JsonManager import *

def main():
    cm = JsonManager(os.path.join(os.path.dirname(
        __file__), "config_params.json"))
    print("Comandos disponibles:")
    print("1. MACs palancas")
    print("2. MAC referenciales")
    print("3. Angulo inicial de referencia")
    print("4. Delta entre mediciones")
    while True:
        try:
            command = int(input("Favor ingresar comando:\n"))
        except ValueError:
            print("favor ingrese un entero")
            continue
        else:
            break

    if (command == 1):
        while True:
            try:
                mac_count = int(input("Favor ingresar numero de MACs:\n"))
            except ValueError:
                print("favor ingrese un entero")
                continue
            else:
                break
        mac_array = []
        for i in range(0, mac_count):
            item = input("Ingrese MAC N " + str(i) + "\n")
            mac_array.append(item)

        cm.data["lever_MACs"] = mac_array
        cm.updateJsonFile()
        print("archivo editado")

    elif(command == 2):
        while True:
            try:
                mac_count = int(input("Favor ingresar numero de MACs:\n"))
            except ValueError:
                print("favor ingrese un entero")
                continue
            else:
                break
        mac_array = []
        for i in range(0, mac_count):
            item = input("Ingrese MAC N " + str(i) + "\n")
            mac_array.append(item)

        cm.data["reference_MACs"] = mac_array
        cm.updateJsonFile()
        print("archivo editado")

    elif(command == 3):
        pass

    elif(command == 4):
        while True:
            try:
                delta = int(input("Favor ingresar tiempo en milisegundos"))
            except ValueError:
                print("favor ingrese un entero")
                continue

        cm.data["time_delta"] = delta
        cm.updateJsonFile()
        print("archivo editado")

    else:
        print("Error: comando no encontrado")


if __name__ == "__main__":
    main()
